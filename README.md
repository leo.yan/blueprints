# Blueprints Landing Page

SOAFEE Blueprints is an initiative driven by SOAFEE's members to materialize the concepts behind SOAFEE's vision of how to enable the software defined vehicle (SDV). This is done by deploying domain-specific applications in a way which leverage features (e.g., containerization) which SOAFEE envisions necessary to realize the SDV.

One important aspect of a blueprint is for a user to be able to reproduce the blueprint on their end. This requires well written and very clear documentation. A user can consume a blueprint as is, for inspiration or as a foundation on which to make modifications which suit their needs.

## Blueprint Structure
A blueprint consists of the following:
* an domain-specific application
* a SOAFEE reference implementation
* standards-based firmware
* hardware; edge, cloud or virtual

<img src="./images/blueprint_generic.jpg" alt="blueprint_generic" width="600"/>

## Best Practices
Placeholder

## Blueprints Tracker
| Release Date | Blueprint | Version |
| ------------ | --------- | ------- |
| TBD | [Autoware Open AD Kit](https://gitlab.com/soafee/blueprints/-/tree/main/Blueprint%20-%20Autoware%20Open%20AD%20Kit) | [V1](https://gitlab.com/soafee/blueprints/-/tree/main/Blueprint%20-%20Autoware%20Open%20AD%20Kit/V1) |
